<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
    <head>
        <title>Home</title>
    </head>
    <body>
        <sec:authorize access="isAnonymous()">
        <h3>Bienvenido</h3>
        <a href="/login">Entrar</a>
        </sec:authorize>
        
        <sec:authorize access="isAuthenticated()">
            Usuario Loggeado
            <a href="/logout">Salir</a>
        </sec:authorize>
            
        <sec:authorize access="hasRole('USER')">
            <h3>Bienvenido
            <sec:authentication property="principal.username"></sec:authentication>
            </h3>
        </sec:authorize>
            
        <sec:authorize access="hasRole('ADMIN')">
            Crear Empleados
        </sec:authorize>
    </body>
</html>
