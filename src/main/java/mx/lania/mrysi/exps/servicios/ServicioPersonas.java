package mx.lania.mrysi.exps.servicios;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import mx.lania.mrysi.exps.entidades.Persona;
import mx.lania.mrysi.exps.repositorios.RepositorioPersonas;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 *
 * @author jaguilar
 */
@Service
public class ServicioPersonas {

    private RepositorioPersonas repoPersonas;

    public ServicioPersonas(RepositorioPersonas repoPersonas) {
        this.repoPersonas = repoPersonas;
    }

    public Persona guardarPersona(Persona persona) {

        if (!StringUtils.hasText(persona.getNombres())) {
            throw new ReglaDeNegociosException("Es obligatorio el nombre");
        }        
        if (!StringUtils.hasText(persona.getApellidoPaterno())) {
            throw new ReglaDeNegociosException("Es obligatorio el apellido paterno");
        }        
        if (!StringUtils.hasText(persona.getCurp())) {
            throw new ReglaDeNegociosException("Es obligatoria la CURP");
        }
        List<Persona> pers = repoPersonas.findByCurp(persona.getCurp());
        if (! pers.isEmpty()) {
            throw new ReglaDeNegociosException("No se puede repetir el CURP");
        }
        
        persona.setFechaRegistro(LocalDate.now());

        return repoPersonas.save(persona);
    }
    
    public Persona actualizarPersona(Persona persona) {
        Optional<Persona> opPersona = repoPersonas.findById(persona.getIdPersona());
        if (opPersona.isPresent()) {
            persona.setFechaRegistro(opPersona.get().getFechaRegistro());
            persona.setCurp(opPersona.get().getCurp());
            repoPersonas.save(persona);
            return persona;
        }
        else {
            throw new DataIntegrityViolationException("No hay persona con id " + persona.getIdPersona());
        }
    }

}
