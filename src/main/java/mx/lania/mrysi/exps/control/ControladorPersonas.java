package mx.lania.mrysi.exps.control;

import java.net.URI;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import mx.lania.mrysi.exps.entidades.Persona;
import mx.lania.mrysi.exps.repositorios.RepositorioPersonas;
import mx.lania.mrysi.exps.servicios.ServicioPersonas;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author jaguilar
 */
@RestController
public class ControladorPersonas {
    
    RepositorioPersonas repoPersonas;
    ServicioPersonas servPersonas;

    public ControladorPersonas(RepositorioPersonas repoPersonas,
            ServicioPersonas servPersonas) {
        this.repoPersonas = repoPersonas;
        this.servPersonas = servPersonas;
    }
    
    @GetMapping("/personas")
    public List<Persona> getPersonas() {
        return repoPersonas.findAll();
    }
    
    @GetMapping("/personas/{curp}")
    public ResponseEntity<Persona> getPersonaPorId(@PathVariable("curp") String curp) {
        List<Persona> personas = repoPersonas.findByCurp(curp);
        if (!personas.isEmpty()) {
            return ResponseEntity.ok(personas.get(0));
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }
    
    @GetMapping(value="/personas", params={"fecha_registro_desde","fecha_registro_hasta"})
    public List<Persona> getPersonasPorFechaRegistro(
            @RequestParam("fecha_registro_desde") LocalDate desde,
            @RequestParam("fecha_registro_hasta") LocalDate hasta,
            @RequestParam(value="apellido_paterno", required = false) String nombre 
    ) {
        return repoPersonas.findByFechaRegistroBetween(desde, hasta);
    }
    
    @GetMapping(value="/personas/busqueda")
    public List<Persona> buscarPersonas(
            @RequestBody ParametrosBusquedaPersonas parametros
    ) {
        return Collections.EMPTY_LIST;
    }
    
    @PostMapping("/personas")
    public ResponseEntity<Persona> guardarPersona(@RequestBody Persona persona) {
        try {
            Persona persNueva = servPersonas.guardarPersona(persona);
            URI uriNuevoExpediente = ServletUriComponentsBuilder
                    .fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(persNueva.getIdPersona())
                    .toUri();
            return ResponseEntity.created(uriNuevoExpediente)
                    .body(persNueva);
        }
        catch(Exception ex) {
            return ResponseEntity.badRequest()
                    .header("ERROR", ex.getMessage())
                    .build();
        }
    }
    
    @PutMapping("/personas/{id}")
    public ResponseEntity<Persona> actualizarPersona(@RequestBody Persona persona,
            @PathVariable("id") Integer id) {
        try {
            Persona persNueva = servPersonas.actualizarPersona(persona);
            return ResponseEntity.ok(persNueva);
        }
        catch(Exception ex) {
            return ResponseEntity.badRequest()
                    .header("ERROR", ex.getMessage())
                    .build();
        }
    }
}