package mx.lania.mrysi.exps.control;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import mx.lania.mrysi.exps.entidades.Expediente;
import mx.lania.mrysi.exps.repositorios.RepositorioExpedientes;
import mx.lania.mrysi.exps.servicios.ServicioExpedientes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author jaguilar
 */
@RestController
@CrossOrigin()
public class ControladorExpedientes {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ControladorExpedientes.class);
    
    RepositorioExpedientes repoExpedientes;
    ServicioExpedientes servExpedientes;

    public ControladorExpedientes(RepositorioExpedientes repoExpedientes,
            ServicioExpedientes servExpedientes) {
        this.repoExpedientes = repoExpedientes;
        this.servExpedientes = servExpedientes;
    }
    
    
    @GetMapping("/expedientes")
    public List<Expediente> getTodosExpedientes() {
        return repoExpedientes.findAll();
    }
    
    @GetMapping("/expedientes/{id}")
    public ResponseEntity<Expediente> getExpedientePorId(@PathVariable("id") Integer idExp) {
        Optional<Expediente> opExp = repoExpedientes.findById(idExp);
        if (opExp.isPresent()) {
            return ResponseEntity.ok(opExp.get());
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }
    
    @PostMapping("/expedientes")
    public ResponseEntity<Expediente> guardarExpediente(@RequestBody Expediente exp) {
        try {
            Expediente expNuevo = servExpedientes.guardar(exp);
            URI uriNuevoExpediente = ServletUriComponentsBuilder
                    .fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(expNuevo.getIdExpediente())
                    .toUri();
            return ResponseEntity.created(uriNuevoExpediente)
                    .body(expNuevo);
        }
        catch(Exception ex) {
            LOGGER.warn("Error al insertar expediente", ex);
            //LOGGER.error("Error al insertar expediente", ex);
            return ResponseEntity.badRequest()
                    .header("ERROR", ex.getMessage())
                    .build();
        }
    }
    
    @PutMapping("/expedientes/{id}")
    public ResponseEntity<Expediente> actualizarExpediente(
            @PathVariable("id") Integer id,
            @RequestBody Expediente exp) {
        try {
            exp.setIdExpediente(id);
            return ResponseEntity.ok(servExpedientes.actualizar(exp));
        }
        catch(Exception ex) {
            return ResponseEntity.badRequest()
                    .header("ERROR", ex.getMessage())
                    .build();
        }
    }
}
