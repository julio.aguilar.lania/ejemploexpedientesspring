package mx.lania.mrysi.exps.entidades;

import com.fasterxml.jackson.annotation.JsonView;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import mx.lania.mrysi.exps.control.Vistas;

/**
 *
 * @author jaguilar
 */
@Entity
@Table(name = "personas")
public class Persona implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_persona")
    @JsonView(Vistas.Minima.class)
    private Integer idPersona;
    
    @Basic(optional = false)
    @Column(name = "nombres")
    @JsonView(Vistas.Media.class)
    private String nombres;
    
    @Basic(optional = false)
    @Column(name = "apellido_paterno")
    @JsonView(Vistas.Media.class)
    private String apellidoPaterno;
    
    @Column(name = "apellido_materno")
    @JsonView(Vistas.Media.class)
    private String apellidoMaterno;
    
    @Basic(optional = false)
    @Column(name ="curp")
    @JsonView(Vistas.Minima.class)
    private String curp;
    
    @Basic(optional = false)
    @Column(name = "fecha_nacimiento")
    private LocalDate fechaNacimiento;
    
    @Basic(optional = false)
    @Column(name = "fecha_registro")
    private LocalDate fechaRegistro;
    
    public Persona() {
    }

    
    public Persona(Integer idPersona, String nombres, String apellidoPaterno, String curp, LocalDate fechaNacimiento, LocalDate fechaRegistro) {
        this.idPersona = idPersona;
        this.nombres = nombres;
        this.apellidoPaterno = apellidoPaterno;
        this.curp = curp;
        this.fechaNacimiento = fechaNacimiento;
        this.fechaRegistro = fechaRegistro;
    }

    
    
    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public LocalDate getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(LocalDate fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

}
