package mx.lania.mrysi.exps.repositorios;

import java.util.List;
import mx.lania.mrysi.exps.entidades.Empleado;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author jaguilar
 */
public interface RepositorioEmpleados extends JpaRepository<Empleado, Integer>{

    public List<Empleado> findByLogin(String login);
    
}
